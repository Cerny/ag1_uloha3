#include <iostream>

using namespace std;


struct linkedItem {
    int id = 0;
    linkedItem *next = nullptr;
};
struct myQueue {
    linkedItem *first = nullptr, *last = nullptr;
    bool isEmpty = true;
    unsigned int size = 0;

    void push(int id) {
        auto *tmpItem = new linkedItem();
        tmpItem->id = id;
        tmpItem->next = nullptr;

        if (isEmpty)
            first = tmpItem;
        else
            last->next = tmpItem;

        last = tmpItem;

        size++;
        isEmpty = false;
    }

    void pop() {
        if (size > 0) {
            if (size > 1) {
                first = first->next;
                if (size == 2)
                    last = first;
            } else {
                first = nullptr;
                last = nullptr;
            }
            size--;
        }

        if (size == 0)
            isEmpty = true;
    }

    void removeIndex(int index) {
        if (index == top()) {
            pop();
            return;
        }

        if (size > 1) {
            linkedItem *prev = first;
            while (prev != nullptr) {
                linkedItem *act = prev->next;

                // index was founded
                if (act->id == index) {
                    prev->next = act->next;

                    size--;
                    if (size == 0)
                        isEmpty = true;

                    // it was the last item
                    if (act->next == nullptr)
                        last = prev;

                    return;
                }

                prev = act;
            }
        }
    }

    int top() {
        if (first == nullptr)
            return -1;

        return first->id;
    }

    int topPop() {
        if (size == 0)
            return -1;

        int top = first->id;
        pop();
        return top;
    }
};
struct street {
    unsigned int from = 0, to = 0, index = 0;
    unsigned long length = 0;

    street *next = nullptr;
};

struct square {
    street *streetsLinkedList = nullptr;
    myQueue *neighborsInSpanningTree = new myQueue;
    bool tradesAreHere = false;

    void addStreet(unsigned int from, unsigned int to, unsigned long length, unsigned int index) {
        auto *newStreet = new street;
        newStreet->from = from;
        newStreet->to = to;
        newStreet->length = length;
        newStreet->index = index;

        if (streetsLinkedList != nullptr)
            newStreet->next = streetsLinkedList;

        streetsLinkedList = newStreet;
    }

    bool visited = false;
};

/*
bool printTest = true;
void print  ( square ** squaresArr, int squaresCnt ) {
    for (int i = 0; i < squaresCnt; i++) {
        cout << "squareIndex:" << i << ", visited: " << squaresArr[i]->visited << ' ';
        street *str = squaresArr[i]->streetsLinkedList;
        while (str != nullptr) {
            cout << "{to:" << str->to << ", len:" << str->length << "},";
            str = str->next;
        }
        cout << endl;
    }
}
*/

street *copyPointer(street *original) {
    auto *streetCopy = new street;
    streetCopy->to = original->to;
    streetCopy->from = original->from;
    streetCopy->next = original->next;
    streetCopy->length = original->length;
    streetCopy->index = original->index;

    return streetCopy;
}

void printIntLinkedList(myQueue *queue) {
    auto *act = queue->first;

    while (act != nullptr) {
        cout << act->id << " ";
        act = act->next;
    }
}

void addIncidentStreetsToStack(street **streetsStack, square *actSquare, square **squaresArr) {
    // add all incident edges to streetsStack
    street *actStreet = actSquare->streetsLinkedList;
    while (actStreet != nullptr) {
        // if the destination square is already visited -> continue loop
        if (squaresArr[actStreet->to]->visited) {
            actStreet = actStreet->next;
            continue;
        }

        street *streetCopy = copyPointer(actStreet);

        if (streetsStack != nullptr)  // not empty stack
            streetCopy->next = *streetsStack;
        else                            // empty stack
            streetCopy->next = nullptr;
        if (streetsStack != nullptr)
            *streetsStack = streetCopy;

        actStreet = actStreet->next;
    }
}

street *findShortestInStack(street *streetsStack, square **squaresArr) {
    // take the shortest street
    street *actStreet = streetsStack;
    street *shortestInStack = nullptr;
    while (actStreet != nullptr) {
        if (!squaresArr[actStreet->to]->visited) {
            if (shortestInStack == nullptr || actStreet->length < shortestInStack->length)
                shortestInStack = actStreet;
        } else {
            // todo remove from avl
        }

        actStreet = actStreet->next;
    }

    return shortestInStack;
}

void setSquare ( square ** squaresArr, int leafId, myQueue * squaresOutputLinkedList, myQueue * leafsQueue  ){
    int xId = squaresArr[leafId]->neighborsInSpanningTree->topPop(); // leaf has just one neighbour
    if (!squaresArr[xId]->tradesAreHere) {
        squaresOutputLinkedList->push(xId); // add x to output
        squaresArr[xId]->tradesAreHere = true;
    }
    squaresArr[xId]->neighborsInSpanningTree->removeIndex(leafId); // remove leaf from x

    // if x has 1 incident edge (y), remove x from y
    if (squaresArr[xId]->neighborsInSpanningTree->size == 1) {
        int yId = squaresArr[xId]->neighborsInSpanningTree->top();

        int yIncidentStreets = squaresArr[yId]->neighborsInSpanningTree->size;
        if ( yIncidentStreets == 2 ) {
            if (squaresArr[yId]->tradesAreHere)
                setSquare(squaresArr, xId, squaresOutputLinkedList, leafsQueue);
            else {
                squaresArr[xId]->neighborsInSpanningTree->pop(); // remove y from x
                squaresArr[yId]->neighborsInSpanningTree->removeIndex(xId); // remove x from y
                leafsQueue->push(yId);
            }
        }
        else if ( yIncidentStreets > 2 || yIncidentStreets == 1) {
            squaresArr[xId]->neighborsInSpanningTree->pop(); // remove y from x
            squaresArr[yId]->neighborsInSpanningTree->removeIndex(xId); // remove x from y
        }
    }
}

int main() {
    // variables
    street *streetsStack = nullptr;
    unsigned int squaresCnt, streetsCnt;

    // readInput
    cin >> squaresCnt >> streetsCnt;
    auto **squaresArr = new square *[squaresCnt];
    for (unsigned int i = 0; i < squaresCnt; i++) {
        squaresArr[i] = new square;
    }

    unsigned int from, to;
    unsigned long length;
    for (unsigned int i = 0; i < streetsCnt; i++) {
        cin >> from >> to >> length;
        squaresArr[from]->addStreet(from, to, length, i);
        squaresArr[to]->addStreet(to, from, length, i);
    }
    // readInput end

    auto *outputStreets = new myQueue;

    unsigned int visitedSquaresCnt = 0;
    unsigned long long sumLength = 0;

    square *actSquare = squaresArr[0];
    while (true) {
        actSquare->visited = true;
        visitedSquaresCnt++;

        if (visitedSquaresCnt == squaresCnt)
            break;

        addIncidentStreetsToStack(&streetsStack, actSquare, squaresArr);
        street *shortestInStack = findShortestInStack(streetsStack, squaresArr);
        outputStreets->push(shortestInStack->index);

        squaresArr[shortestInStack->from]->neighborsInSpanningTree->push(shortestInStack->to);
        squaresArr[shortestInStack->to]->neighborsInSpanningTree->push(shortestInStack->from);

        sumLength += shortestInStack->length;

        actSquare = squaresArr[shortestInStack->to];
    }


    auto *leafsQueue = new myQueue;
    for (unsigned int i = 0; i < squaresCnt; i++) {
        if (squaresArr[i]->neighborsInSpanningTree->size == 1)
            leafsQueue->push(i);
    }

    auto *squaresOutputLinkedList = new myQueue;

    while (leafsQueue->size > 0) {
        int leafId = leafsQueue->topPop();

        if ( squaresArr[leafId]->neighborsInSpanningTree->size == 0 )
            continue;

        // (leafId) - (xId) - (yId)
        // add x to output
        // remove leaf from x
        // if x has 1 incident edge (y), remove x from y
        setSquare(squaresArr, leafId, squaresOutputLinkedList, leafsQueue);
    }


    // print output
    cout << sumLength << " " << squaresOutputLinkedList->size << endl;
    printIntLinkedList(outputStreets);
    cout << endl;
    printIntLinkedList(squaresOutputLinkedList);

    return 0;
}